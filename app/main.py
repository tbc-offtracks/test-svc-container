import os
import re

from fastapi import FastAPI, HTTPException, Query
from fastapi.responses import PlainTextResponse

app = FastAPI()


@app.get("/ping", response_class=PlainTextResponse)
def ping():
    return "pong"


@app.get("/env", response_class=PlainTextResponse)
def get_env():
    return "\n".join([f"{var}={os.environ[var]}" for var in os.environ])


@app.get("/env/{var}", response_class=PlainTextResponse)
def get_env(var:str):
    return os.getenv(var)